package com.alibaba.securityoauth2.common.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * @author Thinkpad
 * @Title: SecurityConfig
 * @ProjectName tadpolecloud
 * @Description: TODO
 * @date 2019/2/2814:58
 * @Version: 1.0
 */
//@Configuration
@EnableWebSecurity
public class SecurityConfig  extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // TODO Auto-generated method stub
        //super.configure(http);
        /*http
                .formLogin().loginPage("/login").loginProcessingUrl("/login/form").failureUrl("/login-error").permitAll()  //表单登录，permitAll()表示这个不需要验证 登录页面，登录失败页面
                .and()
                .authorizeRequests().anyRequest().authenticated()
                .and()
                .csrf().disable();*/
        http
                .authorizeRequests()
                /*.antMatchers(
                        StaticParams.PATHREGX.API,
                        StaticParams.PATHREGX.CSS,
                        StaticParams.PATHREGX.JS,
                        StaticParams.PATHREGX.IMG).permitAll()*///允许用户任意访问
                .anyRequest().authenticated()//其余所有请求都需要认证后才可访问
                .and()
                .formLogin()
                //.loginPage("/login/login.do")  /
                //.defaultSuccessUrl("/hello2")
                .permitAll();//允许用户任意访问
        http.csrf().disable();
    }
}
